import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Hanoi hanoi = new Hanoi();
        System.out.println("HANOI TOWERS GAME!");
        System.out.println("How many elements do you want?");
        int elements = sc.nextInt();
        hanoi.fillStack(elements);
        System.out.println("Hanoi towers created!");
        hanoi.printHanoi();
        System.out.println();
        int movesCounter = 0;

        do{
            System.out.println("From: ");
            int from = sc.nextInt();
            System.out.println("To: ");
            int to = sc.nextInt();
            hanoi.transferInt(from, to);
            hanoi.printHanoi();
            System.out.println();
            movesCounter++;
        } while (!hanoi.isOver());

        System.out.println("Bravo! Game finished in " + movesCounter + " moves.");
    }
}
