import java.util.Stack;

public class Hanoi {
    private Stack<Integer> col1 = new Stack<>();
    private Stack<Integer> col2 = new Stack<>();
    private Stack<Integer> col3 = new Stack<>();

    public void fillStack(int size) {
        for (int i = size; i >= 1; i--) {
            col1.push(i);
        }
    }

    public void transferInt(int from, int to) {

        Stack<Integer> fromStack = col1;
        if (from == 1) fromStack = col1;
        else if (from == 2) fromStack = col2;
        else if (from == 3) fromStack = col3;
        else System.out.println("Wrong column.");

        Stack<Integer> toStack = col2;
        if (to == 1) toStack = col1;
        else if (to == 2) toStack = col2;
        else if (to == 3) toStack = col3;
        else System.out.println("Wrong column");

        if(fromStack.empty()) {
            System.out.println("Can't transfer from an empty stack!");
            return;
        }
        if (toStack.empty() || fromStack.peek() < toStack.peek()) {
            toStack.push(fromStack.pop());
        } else {
            System.out.println("Can't transfer bigger int onto smaller int.");
        }
    }

    public void printHanoi() {
        System.out.println("[COL1] " + col1);
        System.out.println("[COL2] " + col2);
        System.out.println("[COL3] " + col3);
    }

    public boolean isOver() {
        if (col1.empty() && col2.empty()) {
            return true;
        } else {
            return false;
        }
    }

}
